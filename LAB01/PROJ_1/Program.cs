﻿using System;

namespace PROJ_1
{
    class Program
    {
        static bool IsPrime(int a)
        {
            if (a < 2)
                return false;
            for (int i = 2; i * i <= a; i++)
            {
                if (a % i == 0)
                    return false;
            }
            return true;
        }

        static int Factorial(int a)
        {
            if (a < 2)
            {
                return 1;
            }
            else
                return Factorial(a - 1) * a;
        }

        static void PrintFactorial(int a)
        {
            if(a<0)
            {
                Console.WriteLine("Error! Cannot compute factorial of a negatove number: " + a);
            }
            else
                Console.WriteLine("Factorial of " + a + " is equal to: " + Factorial(a));
        }
        static void Main(string[] args)
        {
            for (int i = 0; i < 101; i++)
            {
                if (IsPrime(i))
                    Console.WriteLine(i);
            }

            Console.WriteLine(Factorial(5));

            PrintFactorial(5);
            PrintFactorial(-7);
        }
    }
}