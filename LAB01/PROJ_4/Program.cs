﻿using System;
using System.Collections.Generic;

namespace PROJ_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tab = { "jeden", "dwa", "trzy", "cztery", "pięć" };

            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }

            Console.WriteLine("###");

            foreach (string elem in tab)
            {
                Console.WriteLine(elem);
            }

            List<string> myList = new List<string>()
            {
                "AA",
                "BBB",
                "CCCC",
                "DDD",
                "EE"
            };

            for (int i = 0; i < myList.Count; i++)
            {
                Console.WriteLine(myList[i]);
            }

            Console.WriteLine("###");

            foreach (string item in myList)
            {
                Console.WriteLine(item);
            }

            myList.Add("Added");
            myList.Insert(3, "Inserted");
            myList.Remove("AA");
            myList.RemoveAt(1);

            Console.WriteLine("########");
            foreach (string item in myList)
            {
                Console.WriteLine(item);
            }

            Dictionary<string, int> myDict = new Dictionary<string, int>()
            {
                {"Rajpold",296825}
            };

            myDict.Add("Jacek", 290129);
            myDict.Add("Tomek", 289128);

            foreach (KeyValuePair<string, int> pair in myDict)
            {
                Console.WriteLine("Surname: {0} Index: {1}", pair.Value, pair.Key);
            }

            myDict.Remove("Jacek");
            Console.WriteLine("########");

            foreach (KeyValuePair<string, int> pair in myDict)
            {
                Console.WriteLine("Surname: {0} Index: {1}", pair.Value, pair.Key);
            }

        }
    }
}
