﻿using System;

namespace PROJ_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var myTriangle1 = new RightTriangle();
            var myTriangle2 = new RightTriangle();
            var myTriangle3 = new RightTriangle();

            myTriangle1.A = 5;
            myTriangle1.B = 6;
            Console.WriteLine(myTriangle1.ComputeSine());
        }
    }
}
