using System;

namespace PROJ_3
{
    public class Album
    {
        public string Title
        {
            get;
            set;
        }

        public string Artist
        {
            get;
            set;
        }

        public int Tracks
        {
            get;
            set;
        }

        public int Year
        {
            get;
            set;
        }

        static public int HowManyAlbums
        {
            get;
            set;
        }

        public Album()
        {
            Title = "Unknown";
            Artist = "Unknown";
            Tracks = 0;
            Year = 0;
            HowManyAlbums++;
        }

        public Album(int Tracks_a, int Year_a)
        {
            if (Tracks_a > 0)
            {
                Tracks = Tracks_a;
            }
            if (Year_a > 1800 && Year_a < 2021)
            {
                Year = Year_a;
            }
            HowManyAlbums++;
        }

        public Album(string Title_a, string Artist_a, int Tracks_a, int Year_a)
        {

            Title = Title_a;
            Artist = Artist_a;
            if (Tracks_a > 0)
            {
                Tracks = Tracks_a;
            }
            if (Year_a > 1800 && Year_a < 2021)
            {
                Year = Year_a;
            }
            HowManyAlbums++;
        }

        public void ShowAlbumInfo()
        {
            Console.WriteLine("Title: " + Title + "\n" + "Artist: " + Artist + "\n" + "Tracks: " + Tracks + "\n" + "Year: " + Year);
        }

        static public void ClearAlbumCounter()
        {
            HowManyAlbums = 0;
        }
    }
}