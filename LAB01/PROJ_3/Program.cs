﻿using System;

namespace PROJ_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Album myNewAlbum = new Album()
            {
                Year = 2002,
                Tracks = 10
            };

            myNewAlbum.ShowAlbumInfo();

            var myNewAlbum1 = new Album(10, 2002);
            myNewAlbum1.ShowAlbumInfo();

            var myNewAlbum2 = new Album("Siema", "KASA", -123, 2002);
            myNewAlbum2.ShowAlbumInfo();

            Console.WriteLine("Całkowita liczba albumów to: "+ Album.HowManyAlbums);
            Album.ClearAlbumCounter();
            Console.WriteLine("A teraz to: " + Album.HowManyAlbums);
        }
    }
}
