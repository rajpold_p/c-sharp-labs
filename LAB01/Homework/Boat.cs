namespace Homework
{
    public class Boat
    {
        private int _displacement;
        private string _brand;
        private string _model;
        public int Displacement
        {
            get { return _displacement; }
            set { _displacement = value; }
        }

        public Boat(string brand, string model)
        {
            _brand = brand;
            _model = model;
        }
        public Boat(string brand, string model, int displacement)
        {
            _brand = brand;
            _model = model;
            Displacement = displacement;
        }

    }
}