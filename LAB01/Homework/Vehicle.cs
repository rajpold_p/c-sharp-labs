namespace Homework
{
    public class Vehicle
    {
        public Car Car
        {
            set;
            get;
        }

        public Boat Boat
        {
            get;
            set;
        }

        public static int vehiclesNumber
        {
            get;
            set;
        }

        public Vehicle(string brand, string model, string opt)
        {
            if (opt == "boat")
            {
                Boat = new Boat(brand, model);
            }
            else if (opt == "car")
            {
                Car = new Car(brand, model);
            }
            vehiclesNumber++;
        }

        public int HomManyVehicles()
        {
            return vehiclesNumber;
        }
    }
}