﻿using System;
using System.Collections.Generic;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            var myCar1 = new Car(50000, "Opel", "Astra", 2010, "Green");
            var myCar2 = new Car(25000, "Nissan", "Note", 2015, "White");
            var myCar3 = new Car(22000, "Seat", "Leon", 2012, "Red");

            List<Car> carList = new List<Car>()
            {
                myCar1,
                myCar2,
                myCar3
            };

            foreach (Car car in carList)
            {
                Console.WriteLine(car.BrandWithModel);
            }

            var myVehicle1 = new Vehicle("Seat", "Ibiza", "car");
            var myVehicle2 = new Vehicle("Benetti", "Yacht", " boat"); 

            Console.WriteLine(myVehicle1.Car.BrandWithModel);
            Console.WriteLine(Vehicle.vehiclesNumber);
        }
    }
}
