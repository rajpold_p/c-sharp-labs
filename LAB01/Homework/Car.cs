using System;

namespace Homework
{
    public class Car
    {
        private double _price;
        private string _brand;
        private string _model;

        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }
        public string Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }
        public int Year
        {
            get;
            set;
        }
        public string Color
        {
            get;
            set;
        }

        public string BrandWithModel
        {
            get { return Brand + " " + Model; }
        }

        public Car(string brand, string model)
        {
            Brand = brand;
            Model = model;
        }
        public Car(int price, string brand, string model, int year, string color)
        {
            Price = price;
            Brand = brand;
            Model = model;
            Year = year;
            Color = color;
        }

        private double MonthlyInstalment()
        {
            return Price / (12 * 5);
        }

        public void InstalmentCalc()
        {
            Console.WriteLine("Monthly instalment is equal to: " + MonthlyInstalment());
        }
    }
}