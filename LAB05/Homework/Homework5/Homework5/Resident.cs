﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    abstract class Resident 
    {   
        public string Name
        {
            get;
            set;
        }
        public int Beauty
        {
            get;
            set;
        }
        public int Intellect
        {
            get;
            set;
        }
        public int Strength 
        {
            get;
            set;
        }


        public abstract void Eat();
        public abstract void Sleep();
        public abstract void Work();
        public abstract void Complain();
    }
}
