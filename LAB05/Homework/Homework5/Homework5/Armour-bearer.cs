﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class Armour_bearer : Resident
    {
        public Armour_bearer()
        {
            Name = "Armour-bearer Bobby";
            Strength = 60;
            Intellect = 40;
            Beauty = 80;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating mashed potatoes and grilled pork, which my master left on a plate");
        }

        public override void Sleep()
        {
            Console.WriteLine("I'm sleeping 8 hours a day");
        }

        public override void Work()
        {
            Console.WriteLine("I'm helping my master to protect the castle and i'm feeding his horse");
        }
        public override void Complain()
        {
            Console.WriteLine("This sword is so heavy :(");
        }
    }
}
