﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    interface ICastleCitizensAbstractFactory
    {
        Resident CreateStrongest();
        Resident CreatePrettiest();
        Resident CreateSmartest();
    }
}
