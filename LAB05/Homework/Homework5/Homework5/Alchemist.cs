﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class Alchemist : Resident
    {
        public Alchemist()
        {
            Name = "Alchemist Bob";
            Strength = 20;
            Intellect = 90;
            Beauty = 30;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating dirt from books");
        }

        public override void Sleep()
        {
            Console.WriteLine("I'm sleeping 2 hours a day");
        }

        public override void Work()
        {
            Console.WriteLine("I'm preparing potions");
        }
        public override void Complain()
        {
            Console.WriteLine("I don't have ingredientes for potion :(");
        }
    }
}
