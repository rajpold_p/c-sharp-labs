﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class Princess : Resident
    {
        public Princess()
        {
            Name = "Princess Fiona";
            Strength = 30;
            Intellect = 40;
            Beauty = 90;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating vegetables and drinking wine");
        }

        public override void Sleep()
        {
            Console.WriteLine("I sleeping 18 hours a day");
        }

        public override void Work()
        {
            Console.WriteLine("I'm not working. I'm here to sleep and complain");
        }
        public override void Complain()
        {
            Console.WriteLine("I can't find my brush :(");
        }
    }
}