﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class PigFeeder : Resident
    {
        public PigFeeder()
        {
            Name = "PigFeeder Steve";
            Strength = 50;
            Intellect = 5;
            Beauty = 10;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating the same food as pigs.");
        }

        public override void Sleep()
        {
            Console.WriteLine("I'm sleeping 5 hours a day");
        } 

        public override void Work()
        {
            Console.WriteLine("I'm feeding pigs and protect them from wolfs");
        }
        public override void Complain()
        {
            Console.WriteLine("My food smells odd :(");
        }
    }
}
