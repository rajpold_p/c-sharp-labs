﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class PeasantFactory : ICastleCitizensAbstractFactory
    {
        public Resident CreateStrongest()
        {
            return new PigFeeder();
        }
        public Resident CreatePrettiest()
        {
            return new GrainPicker();
        }
        public Resident CreateSmartest()
        {
            return new Thief();
        }
        public Resident CreateDumbest()
        {
            return new PigFeeder();
        }
    }
}
