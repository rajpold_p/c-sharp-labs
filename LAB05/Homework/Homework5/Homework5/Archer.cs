﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class Archer : Resident
    {
        public Archer()
        {
            Name = "Archer Bob";
            Strength = 70;
            Intellect = 70;
            Beauty = 65;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating chicken and carrots");
        }

        public override void Sleep()
        {
            Console.WriteLine("I'm sleeping 9 hours a day");
        }

        public override void Work()
        {
            Console.WriteLine("I'm protecting the castle and making arrows and bows.");
        }
        public override void Complain()
        {
            Console.WriteLine("There is to many trees and too less arrows :(");
        }
    }
}
