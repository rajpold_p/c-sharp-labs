﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class GrainPicker : Resident
    {
        public GrainPicker()
        {
            Name = "GrainPicker Bob";
            Strength = 40;
            Intellect = 30;
            Beauty = 60;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating bread with butter");
        }

        public override void Sleep()
        {
            Console.WriteLine("I'm sleeping 6 hours a day");
        }

        public override void Work()
        {
            Console.WriteLine("I'm collecting grain");
        }
        public override void Complain()
        {
            Console.WriteLine("The taxes are too high :(");
        }
    }
}
