﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class FightersFactory : ICastleCitizensAbstractFactory
    {
        public Resident CreateStrongest()
        {
            return new Knight();
        }
        public Resident CreatePrettiest()
        {
            return new Armour_bearer();
        }
        public Resident CreateSmartest()
        {
            return new Archer();
        }
    }
}
