﻿using System;
using System.Collections.Generic;

namespace Homework5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ICastleCitizensAbstractFactory> myFactory = new List<ICastleCitizensAbstractFactory>();
            myFactory.Add(new FightersFactory());
            myFactory.Add(new PeasantFactory());
            myFactory.Add(new CourtiersFactory());

            List<Resident> strongestsResidents = new List<Resident>();
            List<Resident> prettiestsResidents = new List<Resident>();
            List<Resident> smartestsResidents = new List<Resident>();


            foreach (ICastleCitizensAbstractFactory factory in myFactory)
            {
                prettiestsResidents.Add(factory.CreatePrettiest());
                smartestsResidents.Add(factory.CreateSmartest());
                strongestsResidents.Add(factory.CreateStrongest());      
            }

            Console.WriteLine("#########################");
            Console.WriteLine("Strongest residents");
            Console.WriteLine("#########################");
            foreach (Resident r in strongestsResidents)
            {
                Console.WriteLine(r.Name);
                Console.WriteLine("------------------------------");
                r.Complain();
                r.Eat();
                r.Work();
                Console.WriteLine("------------------------------");
            }

            Console.WriteLine("#########################");
            Console.WriteLine("Smartest residents");
            Console.WriteLine("#########################");
            foreach (Resident r in smartestsResidents)
            {
                Console.WriteLine(r.Name);
                Console.WriteLine("------------------------------");
                r.Complain();
                r.Eat();
                r.Work();
                Console.WriteLine("------------------------------");
            }

            Console.WriteLine("#########################");
            Console.WriteLine("Prettiest residents");
            Console.WriteLine("#########################");
            foreach (Resident r in prettiestsResidents)
            {
                Console.WriteLine(r.Name);
                Console.WriteLine("------------------------------");
                r.Complain();
                r.Eat();
                r.Work();
                Console.WriteLine("------------------------------");
            }
        }
    }
}
