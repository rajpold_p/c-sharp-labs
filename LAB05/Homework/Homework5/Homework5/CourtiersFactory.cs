﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class CourtiersFactory : ICastleCitizensAbstractFactory
    {
        public Resident CreateStrongest()
        {
            return new Equerry();
        }
        public Resident CreatePrettiest()
        {
            return new Princess();
        }
        public Resident CreateSmartest()
        {
            return new Alchemist();
        }
    }
}
