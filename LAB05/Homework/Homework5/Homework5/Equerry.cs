﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class Equerry : Resident
    {
        public Equerry()
        {
            Name = "Equerry Bob";
            Strength = 70;
            Intellect = 60;
            Beauty = 60;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating beef and drinking ale");
        }

        public override void Sleep()
        {
            Console.WriteLine("I sleeping 7 hours a day");
        }

        public override void Work()
        {
            Console.WriteLine("I take care of King's horses");
        }
        public override void Complain()
        {
            Console.WriteLine("Im scared of horses :(");
        }
    }
}
