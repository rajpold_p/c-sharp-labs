﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class Thief : Resident
    {
        public Thief()
        {
            Name = "Thief Bob";
            Strength = 30;
            Intellect = 70;
            Beauty = 25;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating everything what I'm able to steal");
        }

        public override void Sleep()
        {
            Console.WriteLine("I sleeping 14 hours a day");
        }

        public override void Work()
        {
            Console.WriteLine("I'm just stealing stuff");
        }
        public override void Complain()
        {
            Console.WriteLine("Why people put locks on their doors :(");
        }
    }
}
