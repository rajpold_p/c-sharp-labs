﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework5
{
    class Knight : Resident
    {
        public Knight()
        {
            Name = "Knight Bob";
            Strength = 90;
            Intellect = 60;
            Beauty = 70;
        }

        public override void Eat()
        {
            Console.WriteLine("I'm eating mashed potatoes and grilled pork");
        }

        public override void Sleep()
        {
            Console.WriteLine("I'm sleeping 10 hours a day");
        }

        public override void Work()
        {
            Console.WriteLine("I'm not working. I'm here to protect the castle");
        }
        public override void Complain()
        {
            Console.WriteLine("It's so hot and i'm wearing a chainmail :(");
        }
    }
}
