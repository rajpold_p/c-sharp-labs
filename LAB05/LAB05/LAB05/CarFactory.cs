﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LAB05
{
    class CarFactory : /*VehicleFactory,*/ CityTransportFactory
    {
        public override Vehicle Create(string power, int velLimit) => new Car(power, velLimit);
    }
}
