﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LAB05
{
    abstract class CityTransportFactory
    {
        public abstract Vehicle Create(string power, int velLimit);

        public abstract Vehicle CreateFastest();

        public abstract Vehicle CreateCheapest();

        public abstract Vehicle CreatePublic();

    }
}
