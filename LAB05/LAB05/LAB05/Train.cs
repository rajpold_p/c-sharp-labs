using System;

namespace LAB05
{
    public class Train : Vehicle
    {
        override public void ShowData()
        {
            Console.WriteLine("Vehicle type: train");
        }

        public override string ToString()
        {
            return "This is a train.\nPower: " + Power + "\nVelocity limit: " + VelLimit + "km/h";
        }

        public Train(string power, int velLimit)
        {
            base.Power = power;
            base.VelLimit = velLimit;
        }

        public Train()
        {
            base.Power = "electric";
            base.VelLimit = 120;
        }


    }
}