﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LAB05
{
    abstract class VehicleFactory 
    {
        public abstract Vehicle Create();
    }
}
