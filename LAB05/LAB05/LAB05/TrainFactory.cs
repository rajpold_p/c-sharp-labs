﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LAB05
{
    class TrainFactory : /*VehicleFactory,*/ CityTransportFactory
    {
        public override Vehicle Create(string power, int velLimit) => new Train(power, velLimit);
    }
}
