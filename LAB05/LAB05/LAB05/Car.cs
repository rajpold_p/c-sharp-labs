using System;

namespace LAB05
{
    public class Car : Vehicle
    {
        override public void ShowData()
        {
            Console.WriteLine("Vehicle type: car");
        }

        public override string ToString()
        {
            return "This is a car.\nPower: " + Power + "\nVelocity limit: " + VelLimit + "km/h";
        }

        public Car(string power, int velLimit)
        {
            base.Power = power;
            base.VelLimit = velLimit;
            //ToString();
        }

        public Car()
        {
            base.Power = "diesel";
            base.VelLimit = 200;
        }
    }
}