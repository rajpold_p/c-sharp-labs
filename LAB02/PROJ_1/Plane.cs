using System;

namespace PROJ_1
{
    public class Plane : Vehicle
    {
        override public void ShowData()
        {
            Console.WriteLine("Vehicle type: plane");
        }

        public override string ToString()
        {
            return "This is a plane.\nPower: " + Power + "\nVelocity limit: " + VelLimit + "km/h";
        }

        public Plane(string power, int velLimit)
        {
            base.Power = power;
            base.VelLimit = velLimit;
            //ToString();
        }
    }
}