using System;

namespace PROJ_1
{
    public class Train : Vehicle
    {
        override public void ShowData()
        {
            Console.WriteLine("Vehicle type: train");
        }

        public override string ToString()
        {
            return "This is a train.\nPower: " + Power + "\nVelocity limit: " + VelLimit + "km/h";
        }

        public Train(string power, int velLimit)
        {
            base.Power = power;
            base.VelLimit = velLimit;
            //ToString();
        }


    }
}