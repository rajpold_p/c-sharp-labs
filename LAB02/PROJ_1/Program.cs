﻿using System;
using System.Collections.Generic;

namespace PROJ_1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Vehicle> myTransport = new List<Vehicle>();
            myTransport.Add(new Train("Electric", 90));
            myTransport.Add(new Car("Hybrid", 120));
            myTransport.Add(new Plane("Water", 700));

            foreach(Vehicle v in myTransport)
            {
                Console.WriteLine(v.ToString());
            }
            
            myTransport[1].ShowData();

        }
    }
}
