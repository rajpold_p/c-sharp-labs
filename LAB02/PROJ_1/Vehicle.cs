using System;

namespace PROJ_1
{
    abstract public class Vehicle : IVehicle
    {
        private string _power;
        private int _velLimit;

        public string Power
        {
            get { return _power; }
            set { _power = value; }
        }

        public int VelLimit
        {
            get { return _velLimit; }
            set { _velLimit = value; }
        }

        public virtual void ShowData()
        {
            Console.WriteLine("Unspecified vehicle: ");
        }



    }
}