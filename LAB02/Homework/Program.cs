﻿using System;
using System.Collections.Generic;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Club> myClub = new List<Club>();

            myClub.Add(new FootballClub(123, "Mediolan", "San Siro"));
            myClub.Add(new VolleyballClub(15, "Rzeszów", "ResoviaStadium"));
            myClub.Add(new BasketballClub(40, "Chicago", "BullsStaduim"));

            foreach (Club c in myClub)
            {
                Console.WriteLine(c);
            }
        }
    }
}
