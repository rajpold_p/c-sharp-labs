using System;

namespace Homework
{
    abstract public class Club : IClub, IClub2
    {
        private int _noOfPlayers;
        private string _location;
        private string _stadium;

        public int NoOfPlayers
        {
            get { return _noOfPlayers; }
            set { _noOfPlayers = value; }
        }
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }
        public string Stadium
        {
            get { return _stadium; }
            set { _stadium = value; }
        }
        virtual public void ShowClubData()
        {
            Console.WriteLine("Empty");
        }

        virtual public void ShowYearOfEstablish()
        {
            Console.WriteLine("Empty");
        }

    }
}