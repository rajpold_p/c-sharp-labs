using System;

namespace Homework
{
    public class BasketballClub : Club
    {
        override public void ShowClubData()
        {
            Console.WriteLine("Basketball");
        }

        override public void ShowYearOfEstablish()
        {
            Console.WriteLine("1949");
        }
        override public string ToString()
        {
            return "This is basketball club.\nNumber of players: " + NoOfPlayers + "\nLocation: " + Location + "\nStadium: " + Stadium;
        }

        public BasketballClub(int noOfPlayers, string location, string stadium)
        {
            base.NoOfPlayers = noOfPlayers;
            base.Location = location;
            base.Stadium = stadium;
        }
    }
}