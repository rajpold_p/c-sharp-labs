using System;

namespace Homework
{
    public class VolleyballClub : Club
    {
        override public void ShowClubData()
        {
            Console.WriteLine("Volleyball");
        }

        override public void ShowYearOfEstablish()
        {
            Console.WriteLine("1899");
        }


        override public string ToString()
        {
            return "This is volleyball club.\nNumber of players: " + NoOfPlayers + "\nLocation: " + Location + "\nStadium: " + Stadium;
        }

        public VolleyballClub(int noOfPlayers, string location, string stadium)
        {
            base.NoOfPlayers = noOfPlayers;
            base.Location = location;
            base.Stadium = stadium;
        }
    }
}