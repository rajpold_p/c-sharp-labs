using System;

namespace Homework
{
    public class FootballClub : Club
    {

        override public void ShowClubData()
        {
            Console.WriteLine("Football");
        }

        override public void ShowYearOfEstablish()
        {
            Console.WriteLine("1859");
        }
        override public string ToString()
        {
            return "This is football club.\nNumber of players: " + NoOfPlayers + "\nLocation: " + Location + "\nStadium: " + Stadium;
        }

        public FootballClub(int noOfPlayers, string location, string stadium)
        {
            base.NoOfPlayers = noOfPlayers;
            base.Location = location;
            base.Stadium = stadium;
        }

    }
}