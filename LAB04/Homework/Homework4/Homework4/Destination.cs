﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class Destination
    {
        public string Name
        {
            get;
            set;
        }

        public Destination(string name)
        {
            Name = name;
        }
    }
}
