﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class HydrogenEngine : Engine
    {
        public sealed override double Refill(double amount)
        {
            fuel.Amount += amount;
            return amount * 200;
        }

        public sealed override void OneDayOfTravel()
        {
            fuel.Amount -= 50;
            base.CheckSupplies();
        }

    }
}
