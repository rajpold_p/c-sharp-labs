﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class Waste : IStorage
    {
        private string name;
        public double Amount
        {
            get;
            set;
        }

        public Waste(string s)
        {
            name = s;
        }

        public void HowMuchLeft()
        {
            if (name == "Nuclear")
                Console.WriteLine("Nuclear waste stored: " + Amount);
            if (name == "Generic")
                Console.WriteLine("Generic waste stored: " + Amount);
        }

    }
}
