﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    abstract class Human
    {
        public static double MedianSalary = 100;
        private Random random;

        public double Salary
        {
            get;
            set;
        }

        protected Human(double s)
        {
            Salary = s;
            random = new Random((int)DateTime.Now.Ticks);
        }


        public int Breathe()
        {
            return (int)(random.NextDouble() * 1000);
        }

        public double Eat()
        {
            return random.NextDouble();
        }


    }
}
