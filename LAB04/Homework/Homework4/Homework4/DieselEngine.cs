﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class DieselEngine : Engine
    {
        public sealed override double Refill(double amount)
        {
            fuel.Amount += amount;
            return amount*100;
        }

        public sealed override void OneDayOfTravel()
        {
            fuel.Amount -= 30;
            base.CheckSupplies();
        }
    }
}
