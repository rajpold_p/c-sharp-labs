﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    interface IStorage
    {
        double Amount
        {
            get;
            set;
        }

        void HowMuchLeft();
    }
}
