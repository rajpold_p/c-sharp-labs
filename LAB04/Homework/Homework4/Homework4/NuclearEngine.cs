﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class NuclearEngine : Engine
    {
        private Waste nuclearWaste;

        public NuclearEngine() 
        {
            fuel = new Fuel();
            nuclearWaste = new Waste("Nuclear");
        }

        public sealed override double Refill(double amount)
        {
            fuel.Amount += amount;
            double temp = nuclearWaste.Amount * 20;
            nuclearWaste.Amount = 0;
            return amount * 200 + temp;
        }

        public sealed override void OneDayOfTravel()
        {
            fuel.Amount -= 10;
            nuclearWaste.Amount += 30;
            CheckSupplies();
        }

        public sealed override void CheckSupplies()
        {
            fuel.HowMuchLeft();
            nuclearWaste.HowMuchLeft();
        }

    }
}
