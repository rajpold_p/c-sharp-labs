﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class Scientist : Human
    {

        public ScientificProbe myProbe
        {
            get;
            set;
        }
        public Scientist(double s) : base(s) { }

        public void Work()
        {
            myProbe.GatherData();
        }
    }
}
