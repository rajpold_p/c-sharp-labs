﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class OxygenSupply : IStorage
    {
        public double Amount
        {
            get;
            set;
        }
        
        public double MaxCapacity
        {
            get;
            set;
        }

        public OxygenSupply(double capacity)
        {
            MaxCapacity = capacity;
        }

        public void HowMuchLeft()
        {
            Console.WriteLine("Oxygen supply: " + Amount);
        }

    }
}
