﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class NavigationPanel : ITool
    {
        private int currentNumber;
        private List<Destination> ports;
        private List<int> travelDays;

        public int TotalNumber
        {
            get;
            set;
        }

        public NavigationPanel()
        {
            ports = new List<Destination>();
            travelDays = new List<int>();
        }

        public void AddPort(string name, int days)
        {
            ports.Add(new Destination(name));
            travelDays.Add(days);
            TotalNumber++;
        }

        public void Arrived()
        {
            currentNumber++;
        }

        public void Conserve()
        {
            Console.WriteLine("Navigation panel ready");
        }

        public int DaysToNextDestination()
        {
            return travelDays[currentNumber];
        }

        public string NextDestination()
        {
            return ports[currentNumber].Name;
        }

    }
}
