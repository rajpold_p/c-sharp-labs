﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class FoodSupply : IStorage
    {
        public double Amount
        {
            get;
            set;
        }

        public void HowMuchLeft()
        {
            Console.WriteLine("Food supply: " + Amount);
        }

    }
}
