﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    class Fuel : IStorage
    {
        public double Amount
        {
            get;
            set;
        }

        public void HowMuchLeft()
        {
            Console.WriteLine("Fuel supply: " + Amount);
        }
    }
}
