﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    abstract class Engine
    {
        protected Fuel fuel; // tu jest jakas gwiazdka to nwm xdd(chyba protected będzie - w sumie logiczne) 

        public Engine() 
        {
            fuel = new Fuel();
        }

        public abstract double Refill(double amount);

        public abstract void OneDayOfTravel();

        public virtual void CheckSupplies()
        {
            fuel.HowMuchLeft();
        }
    }
}
