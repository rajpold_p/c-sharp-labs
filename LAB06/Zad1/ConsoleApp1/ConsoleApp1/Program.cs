﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ExamQuestion q1 = new ExamQuestion("Jak masz na imię?", 1);
            ExamQuestion q2 = new ExamQuestion("Jak masz na nazwisko?", 1);
            ExamQuestion q3 = new ExamQuestion("Narysować lustro prądowe.", 8);

            QuestionSample s1 = new QuestionSample();
            s1.Add(q1);
            s1.Add(q2);
            s1.Add(q3);
            s1.Ask();
        }
    }
}
