﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class QuestionSample : IQuestion
    {   
        private List<ExamQuestion> questionList = new List<ExamQuestion>();

        public void Add(ExamQuestion question)
        {
            questionList.Add(question);
        }
        public void Ask()
        {
            foreach(ExamQuestion question in questionList)
            {
                question.Ask();
            }
        }
    }
}
