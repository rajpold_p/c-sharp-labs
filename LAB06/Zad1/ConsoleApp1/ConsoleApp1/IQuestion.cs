﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{

    interface IQuestion
    {
        void Ask();
    }
}
