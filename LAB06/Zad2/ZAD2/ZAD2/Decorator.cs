﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD2
{
    abstract class Decorator : IWeatherAlert
    {
        protected IWeatherAlert weatherAlert;
        public string CurrentDate { get; set; }

        public Decorator(IWeatherAlert weatherAlert)
        {
            this.weatherAlert = weatherAlert;
        }

        public abstract void Alert();
    }

    class SnowDecorator : Decorator
    {
        public SnowDecorator(IWeatherAlert weatherAlert) : base(weatherAlert) { }

        public override void Alert()
        {
            weatherAlert.Alert();
            Console.WriteLine("snow,");
        }
    }

    class RainDecorator : Decorator
    {
        public RainDecorator(IWeatherAlert weatherAlert) : base(weatherAlert) { }

        public override void Alert()
        {
            weatherAlert.Alert();
            Console.WriteLine("rain,");
        }
    }

    class WindDecorator : Decorator
    {
        public WindDecorator(IWeatherAlert weatherAlert) : base(weatherAlert) { }

        public override void Alert()
        {
            weatherAlert.Alert();
            Console.WriteLine("wind,");
        }
    }

    class FogDecorator : Decorator
    {
        public FogDecorator(IWeatherAlert weatherAlert) : base(weatherAlert) { }

        public override void Alert()
        {
            weatherAlert.Alert();
            Console.WriteLine("fog,");
        }
    }
}
