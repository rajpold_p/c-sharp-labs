﻿using System;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            IWeatherAlert monday = new RainAlert();
            monday = new WindDecorator(monday);
            IWeatherAlert tuesday = new NoAlert();
            tuesday = new FogDecorator(tuesday);
            IWeatherAlert wednesday = new RainAlert();
            wednesday = new WindDecorator(wednesday);
            wednesday = new SnowDecorator(wednesday);
            monday.Alert();
            tuesday.Alert();
            wednesday.Alert();
        }
    }
}
