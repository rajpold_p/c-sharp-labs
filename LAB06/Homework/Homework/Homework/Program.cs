﻿using System;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            //composite
            IEquipment w1 = new Weapon("Glamdring", "Sword", 100);
            IEquipment w2 = new Weapon("Anguriel", "Bow", 80);
            IEquipment w3 = new Weapon("Grond", "Warhammer", 200);

            IEquipment a1 = new Armor("Steel", 50, 40);
            IEquipment a2 = new Armor("Leather", 70, 50);
            IEquipment a3 = new Armor("Bone plate", 100, 30);

            IEquipment p1 = new Potion("Healing", 50);
            IEquipment p2 = new Potion("Mana", 100);

            Equipment eq = new Equipment();
            eq.Add(w1);
            eq.Add(w2);
            eq.Add(w3);
            eq.Add(a1);
            eq.Add(a2);
            eq.Add(a3);
            eq.Add(p1);
            eq.Add(p2);

            eq.DisplayProperties();


            //decorator
            eq.Remove(w1);
            eq.Remove(w2);
            eq.Remove(w3);
            eq.Remove(a1);
            eq.Remove(a2);
            eq.Remove(a3);
            eq.Remove(p1);
            eq.Remove(p2);
            
            eq.DisplayProperties();

            w1 = new WeaponEnchantment(w1);
            a2 = new ArmorEnchantment(a2);
            p1 = new PotionEnchantment(p1);

            eq.Add(w1);
            eq.Add(a2);
            eq.Add(p1);

            eq.DisplayProperties();
        }
    }
}
