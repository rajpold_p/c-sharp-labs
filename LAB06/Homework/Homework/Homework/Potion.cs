﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework
{
    class Potion : IEquipment
    {
        private int power;
        private string type;

        public Potion(string type, int power)
        {
            this.type = type;
            this.power = power;
        }

        public int Power
        {
            get { return this.power; }
            set { this.power = value; }
        }
        public void DisplayProperties()
        {
            Console.WriteLine("-------------------------");
            Console.WriteLine("#####Potion#####");
            Console.WriteLine("Type: " + this.type);
            Console.WriteLine("Power: " + this.power);
        }
    }
}
