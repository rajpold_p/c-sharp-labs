﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework
{
    class Weapon : IEquipment
    {
        private string name;
        private string type;
        private int power;

        public Weapon(string name, string type, int power)
        {
            this.name = name;
            this.type = type;
            this.power = power;
        }

        public int Power
        {
            get { return this.power; }
            set { this.power = value;  }
        }

        public void DisplayProperties()
        {
            Console.WriteLine("-------------------------");
            Console.WriteLine("#####Weapon#####");
            Console.WriteLine("Name: " + this.name);
            Console.WriteLine("Type: " + this.type);
            Console.WriteLine("Power: " + this.power);
        }
    }
}
