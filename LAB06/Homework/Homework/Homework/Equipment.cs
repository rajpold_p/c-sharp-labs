﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework
{
    class Equipment : IEquipment
    {
        List<IEquipment> eq = new List<IEquipment>();

        public void Add(IEquipment equip)
        {
            eq.Add(equip);
        }

        public void Remove(IEquipment equip)
        {
            eq.Remove(equip);
        }

        public void DisplayProperties()
        {
            Console.WriteLine("############");
            Console.WriteLine("EQUIPMENT: ");
            Console.WriteLine("############");
            foreach (IEquipment equipment in eq)
            {
                equipment.DisplayProperties();
            }
        }
    }
}
