﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework
{
    class Armor : IEquipment
    {
        private string type;
        private int durability;
        private int weight;

        public Armor(string type, int durability, int weight)
        {
            this.type = type;
            this.durability = durability;
            this.weight = weight;
        }

        public int Durability
        {
            get { return this.durability; }
            set { this.durability = value; }
        }

        public void DisplayProperties()
        {
            Console.WriteLine("-------------------------");
            Console.WriteLine("#####Armor#####");
            Console.WriteLine("Type: " + this.type);
            Console.WriteLine("Durability: " + this.durability);
            Console.WriteLine("Weight: " + this.weight);
        }
    }
}
