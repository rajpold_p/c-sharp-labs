﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework
{
    abstract class Enchantment : IEquipment
    {
        protected IEquipment equipment;
        public Enchantment(IEquipment equipment)
        {
            this.equipment = equipment;
        }

        public abstract void DisplayProperties();
    }

    class WeaponEnchantment : Enchantment
    {

        public WeaponEnchantment(IEquipment weapon) : base(weapon) { }
        public override void DisplayProperties()
        {
            equipment.DisplayProperties();
            Console.WriteLine("Enchanted, power increased");
        }
    }

    class PotionEnchantment :Enchantment
    {
        public PotionEnchantment(IEquipment potion) : base(potion) { }
        public override void DisplayProperties()
        {
            equipment.DisplayProperties();
            Console.WriteLine("Potion enchanted, HP/mana recovery increased");
        }
    }

    class ArmorEnchantment : Enchantment
    {
        public ArmorEnchantment(IEquipment armor) : base(armor) { }
        public override void DisplayProperties()
        {
            equipment.DisplayProperties();
            Console.WriteLine("Armor enchanted, durability increased");
        }
    }

}
